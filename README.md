# Présentation
## Services
* Redis          : Serveur Redis
* Redis Sentinel : Service de HA Redis
* Keepalived	    : Gestion de la VRRP

# Mise en place
## Installation
Installer les services ```redis, redis-sentinel, keepalived```

Suivant les distributions, sentinel n'a pas forcement de script d'init, celui-ci est dans le dossier init.d de ce depot.

## Systeme
Dans le dossier /etc/sysctl.d/ 10-keepalived 20-redis contienentles parametres kernel necessaire à leur bon fonctionnement

```
wget -q https://gitlab.com/pad92/redis-ha/-/raw/master/etc/sysctl.d/10-keepalived.conf -N -P /etc/sysctl.d/
wget -q https://gitlab.com/pad92/redis-ha/-/raw/master/etc/sysctl.d/20-redis.conf      -N -P /etc/sysctl.d/
sysctl -p /etc/sysctl.d/10-keepalived.conf 
sysctl -p /etc/sysctl.d/20-redis.conf 
```

## Redis
* sentienl.conf     : motinoring du serveur master "initial" uniquement, la configuration se mettra a jour par la suite
* redis_master.conf : Configuration, a renomer en redis.conf, pour le master
* redis_slave.conf  : Configuraiton, a renomer en redis.conf, pour le(s) slave, ne pas oublier de changer l'ip dans le fichier

## Keepalived
Changer les valeurs des parrametres suivant :
* router_id
* virtual_router_id
* priority 100 pour le master, 101 pour le slave
* virtual_ipaddress
