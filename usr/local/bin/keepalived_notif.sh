#!/bin/sh
MAILTO='user@domain.ltd'

echo "<p>Dernier changement d'etat : $(date +"%Y/%m/%d %H:%M : ") -> <u>$3</u></p>" > /tmp/keepalived.$2.state

case $3 in
    MASTER )
        echo 00 >> /tmp/keepalived.$2.state
        echo "Dernier changement d'etat : $(date +"%Y/%m/%d %H:%M : ") -> $3" | mail -s "$(hostname -s ) - $2 - $3" $MAILTO
    ;;
    BACKUP )
        echo 11 >> /tmp/keepalived.$2.state
        echo "Dernier changement d'etat : $(date +"%Y/%m/%d %H:%M : ") -> $3" | mail -s "$(hostname -s ) - $2 - $3" $MAILTO
    ;;
    FAULT )
        echo 22 >> /tmp/keepalived.$2.state
        echo "Dernier changement d'etat : $(date +"%Y/%m/%d %H:%M : ") -> $3" | mail -s "$(hostname -s ) - $2 - $3" $MAILTO
        pkill -9 keepalived
    ;;
    * )
        echo 44 >> /tmp/keepalived.$2.state
    ;;
esac
